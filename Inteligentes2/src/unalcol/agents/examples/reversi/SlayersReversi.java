
package unalcol.agents.examples.reversi;

import java.util.Random;

import unalcol.agents.Action;
import unalcol.agents.AgentProgram;
import unalcol.agents.Percept;

/**
 *
 * @author Slayers
 */
public class SlayersReversi implements AgentProgram {
	protected String color;
	protected TipoCasilla[][] tablero;
	protected String tiempoEspera;
	protected TipoCasilla micolor;
	protected TipoCasilla enemigo;
	protected int tamano;
	protected int nivelBusqueda;
	protected int[] counter;

	private class resultFindMax {
		int max, nb, nw;
	};

	public SlayersReversi(String color) {
		this.color = color;
		this.tiempoEspera = "";
		this.tamano = 0;
		this.nivelBusqueda = 3;
		this.counter = new int[2];
	}

	@Override
	public Action compute(Percept p) {

		if (tiempoEspera.equals("")) {
			tamano = Integer.parseInt((String) p.getAttribute(Reversi.SIZE));
			tablero = new TipoCasilla[tamano][tamano];
			clear();

			tiempoEspera = (String) (p.getAttribute(Reversi.BLACK + "_" + Reversi.TIME));

			if (color.equals(Reversi.BLACK)) {
				micolor = TipoCasilla.black;
				enemigo = TipoCasilla.white;
			} else {
				micolor = TipoCasilla.white;
				enemigo = TipoCasilla.black;
			}
			
			System.out.println("Slayers: "+color.toUpperCase());

		}

		actualizarTablero(p);

		if (p.getAttribute(Reversi.TURN).equals(color)) {

			Movimiento movimiento = new Movimiento();
			if (!encontrarMovimiento(micolor, nivelBusqueda, movimiento)) {
				return new Action(Reversi.PASS);
			}
			return new Action(movimiento.i + ":" + movimiento.j + ":" + color);
		}
		
		tiempoEspera = (String) (p.getAttribute( micolor  + "_" + Reversi.TIME));
		if(Integer.parseInt(tiempoEspera.split(":")[2])<7){
			nivelBusqueda=2;
		}

		return new Action(Reversi.PASS);

	}

	public void actualizarTablero(Percept p) {
		String dato = "";
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[0].length; j++) {
				dato = (String) p.getAttribute(i + ":" + j);
				if (!dato.equals(Reversi.SPACE)) {
					if (dato.equals(color)) {
						tablero[i][j] = micolor;
					} else {
						tablero[i][j] = enemigo;
					}
				}
			}
		}
	}

	public boolean encontrarMovimiento(TipoCasilla player, int nivel, Movimiento move) {
		TipoCasilla[][] TempBoard = new TipoCasilla[tamano][tamano];
		int min, n_min;
		boolean encontrado;
		resultFindMax res = new resultFindMax();
		Random random = new Random();

		if (counter[0] + counter[1] >= 52 + nivel) {
			nivel = counter[0] + counter[1] - 52;
			if (nivel > 5)
				nivel = 5;
		}

		for (int i = 0; i < tamano; i++) {
			System.arraycopy(tablero[i], 0, TempBoard[i], 0, tamano);
		}
		encontrado = false;
		min = 100000;
		n_min = 1;
		for (int i = 0; i < tamano; i++)
			for (int j = 0; j < tamano; j++)
				if ((tablero[i][j] == TipoCasilla.space) && (checkBoard(new Movimiento(i, j), player) != 0)) {
					if (player == TipoCasilla.black)
						res = FindMax(nivel - 1, TipoCasilla.white, player);
					else
						res = FindMax(nivel - 1, TipoCasilla.black, player);
					if ((!encontrado) || (min > res.max)) {
						min = res.max;
						move.i = i;
						move.j = j;
						encontrado = true;
					} else if (min == res.max) {
						n_min++;
						if (random.nextInt(n_min) == 0) {
							move.i = i;
							move.j = j;
						}
					}

					for (int k = 0; k < tamano; k++)
						System.arraycopy(TempBoard[k], 0, tablero[k], 0, tamano);
				}
		return encontrado;
	}

	private int checkBoard(Movimiento move, TipoCasilla tipo) {
		int j = Check(move, 1, 0, tipo, true);
		j += Check(move, -1, 0, tipo, true);
		j += Check(move, 0, 1, tipo, true);
		j += Check(move, 0, -1, tipo, true);
		j += Check(move, 1, 1, tipo, true);
		j += Check(move, -1, 1, tipo, true);
		j += Check(move, 1, -1, tipo, true);
		j += Check(move, -1, -1, tipo, true);
		return j;
	}

	private int Check(Movimiento move, int incx, int incy, TipoCasilla tipo, boolean set) {

		int x = move.i;
		int y = move.j;
		int n_inc = 0;
		x += incx;
		y += incy;
		while ((x < tamano) && (x >= 0) && (y < tamano) && (y >= 0) && (tablero[x][y] == enemigo)) {
			x += incx;
			y += incy;
			n_inc++;
		}
		if ((n_inc != 0) && (x < tamano) && (x >= 0) && (y < tamano) && (y >= 0) && (tablero[x][y] == micolor)) {
			if (set)
				for (int j = 1; j <= n_inc; j++) {
					x -= incx;
					y -= incy;
				}
			return n_inc;
		} else
			return 0;
	}

	private resultFindMax FindMax(int level, TipoCasilla me, TipoCasilla opponent) {
		int min, score, tnb, tnw;
		TipoCasilla[][] TempBoard = new TipoCasilla[tamano][tamano];
		resultFindMax res = new resultFindMax();
		level--;
		for (int i = 0; i < tamano; i++)
			System.arraycopy(tablero[i], 0, TempBoard[i], 0, tamano);
		min = 100000; // high value

		for (int i = 0; i < tamano; i++)
			for (int j = 0; j < tamano; j++)
				if ((tablero[i][j] == TipoCasilla.space) && (checkBoard(new Movimiento(i, j), me) != 0)) {
					if (level != 0) {
						resultFindMax tres = FindMax(level, opponent, me);
						tnb = tres.nb;
						tnw = tres.nw;
						score = tres.max;
					} else {
						tnb = counter[0];
						tnw = counter[1];
						score = counter[opponent.ordinal() - 1] - counter[me.ordinal() - 1] + strategy(me, opponent);
					}
					if (min > score) {
						min = score;
						res.nb = tnb;
						res.nw = tnw;
					}
					for (int k = 0; k < tamano; k++)
						System.arraycopy(TempBoard[k], 0, tablero[k], 0, tamano);
				}
		res.max = -min;
		return res;
	}

	private int strategy(TipoCasilla me, TipoCasilla opponent) {
		int tstrat = 0;
		for (int i = 0; i < tamano; i++)
			if (tablero[i][0] == enemigo)
				tstrat++;
			else if (tablero[i][0] == micolor)
				tstrat--;
		for (int i = 0; i < tamano; i++)
			if (tablero[i][tamano - 1] == enemigo)
				tstrat++;
			else if (tablero[i][tamano - 1] == micolor)
				tstrat--;
		for (int i = 0; i < tamano; i++)
			if (tablero[0][i] == enemigo)
				tstrat++;
			else if (tablero[0][i] == micolor)
				tstrat--;
		for (int i = 0; i < tamano; i++)
			if (tablero[tamano - 1][i] == enemigo)
				tstrat++;
			else if (tablero[tamano - 1][i] == micolor)
				tstrat--;
		return tstrat;
	}

	class Movimiento {
		int i, j;

		public Movimiento() {
		}

		public Movimiento(int i, int j) {
			this.i = i;
			this.j = j;
		}
	};

	enum TipoCasilla {
		space, black, white
	};

	public void clear() {
		for (int i = 0; i < tamano; i++)
			for (int j = 0; j < tamano; j++)
				tablero[i][j] = TipoCasilla.space;
		counter[0] = 2;
		counter[1] = 2;
	}

	@Override
	public void init() {
		this.tiempoEspera = "";
		this.tamano = 0;
		this.nivelBusqueda = 3;
		this.counter = new int[2];
	}

}
